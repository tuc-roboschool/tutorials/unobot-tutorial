# Befehlsreferenz UNObot

## Inhalt

  1. [Bibliotheksfunktionen](#1-bibliotheksfunktionen)
  2. [LED’s](#2-leds)
  3. [Timer](#3-timer)
  4. [LCD](#4-lcd)
  5. [Taster](#5-taster)
  6. [Helligkeitssensoren](#6-helligkeitssensoren)
  7. [Motoren](#7-motoren)
  8. [Buzzer](#8-buzzer)
  9. [Entfernungssensor](#9-entfernungssensor)


## 1. Bibliotheksfunktionen
Im Folgenden werden nur die Deklarationen der Funktionen angegeben.
Der Aufruf erfolgt, wie oben beschrieben, ohne Angabe der Typen. Alle
im Folgenden vorgestellten Funktionen sind in der Headerdatei UNObot.h
enthalten und können mit

```c
#include "UNObot.h"
```
am Anfang der Datei, in der sie verwendet werden, eingebunden werden.

**WICHTIG**

Die Referenz ist wie folgt aufgebaut: Sie besteht zuerst aus der Deklaration der vorhandenen Funktionen. Diese Deklarationen sollen so, wie
sie hier stehen nicht einfach Copy - Paste in deinem Code erscheinen,
sondern sie dienen nur als Hilfe, damit man sieht, welche Parameter
mit welchen Datentypen die Funktion erwartet und was die Funktion
zurückgibt. Nach der Deklaration folgen Anwendungsbeispiele, die du
dann in jener Form in deinem Code verwenden kannst.

## 2. LED’s

**Deklaration:**
```c
void r_led(boolean state);
void y_led(boolean state);
void g_led (boolean state);
```

**Anwendungsbeispiel:**
```c
r_led(1); // So kannst du die Funktion benutzen
```

Setzt den Zustand der LEDs state: 0 oder LOW zum Ausschalten; 1 oder
HIGH zum Einschalten. r steht dabei für red (rot), y für yellow (gelb) und g
für green (grün).

Außerdem gibt es PWM-Funktionen, mit ihnen lassen sich die Helligkei-
ten regeln. 0 ist dabei aus und 255 ganz hell.

**Deklaration:**
```c
void r_led_pwm(int brightness);
void y_led_pwm(int brightness);
void g_led_pwm(int brightness);
```

**Anwendungsbeispiel:**
```c
r_led_pwm(200); // So kannst du die Funktion benutzen
```

## 3. Timer

**Deklaration:**
```c
void delay(intmilliseconds);
```

**Anwendungsbeispiel:**
```c
delay(1000);    // So kannst du die Funktion benutzen
```

Pausiert die Ausführung des Programms für eine bestimmte Zeit: Dauer in Millisekunden.

## 4. LCD
Um das LCD nutzen zu können, musst du es zunächst initialisieren. Dabei
empfiehlt sich die Initialisierung in der Setup-Funktion durchzuführen, da
diese nur einmal am Anfang aufgerufen wird.
```c
void setup() {
    UNObot_init();  // Immer als erstes in setup() sobald du das Display verwenden willst
}

void clear(); // Deklaration
```

**Anwendungsbeispiel:**
```c
clear();    // Funktionsaufruf
```
Löscht den Display-Inhalt.

**Deklaration:**
```c
void lcd_goto_xy(int x, int y);
```

**Anwendungsbeispiel:**
```c
lcd_goto_xy(0,0); // Funktionsaufruf mit Parameterübergabe
```

Bewegt den Cursor an die Position (x, y). Die obere linke Ecke des Displays
hat die Koordinaten (0, 0). x ist die Spalte, y ist die Zeile.

**Deklaration:**
```c
void print(String string);
void print(int number);
```

**Anwendungsbeispiele:**
```c
print ("Hello");    // Funktionsaufruf mit Parameterübergabe
print (10);         // Funktionsaufruf mit Parameterübergabe
```

Gibt den entsprechenden Wert auf dem Display aus.

**Deklaration:**
```c
void bar(int min, int max, int value, int column, int row);
```

**Anwendungsbeispiel:**
```c
bar(0, 200, 57, 0, 0); // Funktionsaufruf mit Parameterübergabe
```

Mit dem Befehl bar() ist es möglich eine Säule in einem Feld (Kästchen) des
Displays zu zeichnen (praktisch zum auswerten der Helligkeitssensoren).
Wie hoch die Säule wird, berechnet die Funktion automatisch. Sie benötigt
dafür einige Parameter. min und max geben die unteren und oberen Grenzen 
der Säule an. value repräsentiert nun letztendlich den Wert, der dann
auch gezeichnet wird (relativ zu den vorher gesetzten Grenzen). Die letzten
beiden Parameter column und row geben an, in welchem Kästchen auf dem
Display die Säule gezeichnet werden soll (ähnlich wie lcd_goto_xy()).

## 5. Taster

**Deklaration:**
```c
bool wait_for_button(int button);
```

**Anwendungsbeispiel:**
```c
wait_for_button(0); // Funktionsaufruf mit Parameterübergabe
```

Hält die Ausführung des Programms an, bis der übergebene Button ge-
drückt wurde. In dem Funktionsparameter 'button' muss angegeben werden,
auf welchen Button die Funktion reagieren soll. Hier kann folgendes ange-
geben werden: 0, 1, 2. Der Rückgabewert entspricht wahr oder falsch.
Falls der Button noch nicht bekannt ist, auf den gewartet werden soll,
kann man die Funktion wait_for_any_button() nutzen.

**Deklaration:**
```c
int wait_for_any_button();
```

**Anwendungsbeispiel:**
```c
int pressed_button = wait_for_any_button(); // Funktionsaufruf mit Parameterübergabe
```
Der Rückgabewert entspricht dem betätigten Button: entweder 0, 1 oder 2.

## 6. Helligkeitssensoren

**Deklaration:**
```c
void read_line_sensors(int values[]);
```

**Anwendungsbeispiel:**
```c
int values[3];              // Array f ür die 3 Sensoren
read_line_sensors(values);  // Funktionsaufruf mit Parameterübergabe
```

Liest die 3 Helligkeitssensoren des UNObots aus. Die Werte ’ values’ liegen
zwischen 0 (hell) und 1023 (dunkel).

## 7. Motoren

**Deklaration:**
```c
void set_motors(int left_speed, int right_speed);
```

**Anwendungsbeispiel:**
```c
set_motors(30, 30); // Funkionsaufruf mit Parameterübergabe
```

Setzt die Geschwindigkeit der Motoren. Der Parameter 'left_speed' stellt
die Geschwindigkeitsangabe für den linken Motor und 'right_speed' die Ge-
schwindigkeit für den rechten Motor dar. Mit positiven Werten bis zu 255
dreht sich der Motor vorwärts, mit negativen Werten rückwärts und bei 0
bleibt der Motor stehen.

## 8. Buzzer

**Deklaration:**
```c
void buzzer(int frequency, int duration);
```

**Anwendungsbeispiel:**
```c
buzzer (5000, 500); // Funkionsaufruf mit Parameterübergabe
```

## 9. Entfernungssensor

**Deklaration:**
```c
int get_distance();
```

**Anwendungsbeispiel:**
```c
int distanc = get_distance();   // Funkionsaufruf mit Parameterübergabe
```

Die Funktion gibt 0 zurück, falls die Entfernung größer als 3m bzw. kleiner
als 3cm ist. Generell werden die Werte in cm zurückgegeben.
