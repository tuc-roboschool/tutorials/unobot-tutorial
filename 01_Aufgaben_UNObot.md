# Aufgaben für die Programmierung des UNObot

## Der Parcours

Der Parcours dient als ultimative Herausforderung für dich. Damit kannst du zeigen, dass du die Programmierung des Roboters wirklich beherrschst.

Den Parcours kannst du dir nach der [Vorlage](assets/parcours.svg) einfach selbst mit Isolierband auf dem Fußboden abkleben.
Du darfst natürlich auch Änderungen daran vornehmen oder dir eine komplett neue Strecke ausdenken.
In der Vorlage sind außerdem die Schwierigkeitsgrade der einzelnen Abschnitte mit eingetragen.

Diese weiterführenden Aufgaben dienen als vorbereitende Meilensteine für die Bewältigung
des [Parcours](assets/parcours.svg). Sie sollten in der vorliegenden Reihenfolge gelöst werden.

Es bietet sich an für jeden Block einen neuen Sketch in der Arduino IDE zu erstellen. So hast
du einen besseren Überblick über die verschiedenen Themengebiete und
kannst später eventuell noch auf einen Codeabschnitt zurückgreifen.

> **Hinweis**
>
> Mit Zusatz gekennzeichnete Aufgaben sind schwieriger und erfordern,
> dass du dich mit den Funktionen des UNObots intensiver auseinandersetzt. Sie sind nicht zwingend für die Bewältigung des Parcours notwendig und können daher gegebenenfalls weggelassen werden.

## Block I: Abfragen von Buttons & Ansteuern von Motoren
In diesem Abschnitt soll es darum gehen, wie du mithilfe der Bibliothek
Buttons abfragen sowie die Motoren ansteuern kannst.

> **Hinweis**
> Die Verwendung der wichtigsten Befehle für den Roboter wird in der [Befehlsreferenz](02_Referenz.md) erläutert.
> Die erste Zeile deines Programms muss immer #include "UNObot.h" enthalten.

Erstelle einen neuen Sketch und bereite das Grundgerüst jedes Programms vor
(zur Erinnerung: #include "UNObot.h" und UNObot_init() in setup()).

Nun zum eigentlichen Programm. Füge in die (leere) loop() Funktion neue
Anweisungen ein, die der Reihenfolge nach folgende Verhalten umsetzen.
Die für die Umsetzung nützlichen Funktionen sind jeweils in Klammern
notiert.

1. Warte auf einen Tastendruck. (wait_for_button())
2. Warte weitere 500 Millisekunden, damit der Nutzer Zeit hat, den
Finger wegzubewegen.
3. Setze die Motoren so, dass der UNObot vorwärts fährt. Verwende für
den Anfang eine Geschwindigkeit zwischen 40 und 60. (set_motors())
34. Halte nach ca. 20 cm wieder an. Der Roboter kann zwar keine Di-
stanzen messen, mit clever gewählten Geschwindigkeiten und Timing
kann man das aber ausgleichen.

## Block II: Taster unterscheiden mit Alternativen

Schreibe ein Programm, das auf einen Tastendruck wartet und dann, ab-
hängig vom dem gedrückten Taster, den Roboter unterschiedlich fahren
lässt:
+ Wird **Taste A** gedrückt, fährt der UNObot eine scharfe Kurve um 90° nach links.
+ Wird **Taste B** gedrückt, fährt der UNObot ca. 20 cm geradeaus.
+ Wird **Taste C** gedrückt, fährt der UNObot eine scharfe Kurve um 90° nach rechts.

Die Funktion wait_for_button() hält das Programm an der Stelle, an der sie
aufgerufen wurde, an und das Programm wird erst fortfahren wenn der
jeweilige Button gedrückt wurde. Dies kann ungewollte Nebeneffekte hervorrufen,
zum Beispiel wenn vorher ein set_motors() gerufen wurde. Die
Motoren drehen sich trotzdem weiter, auch wenn das Programm an dieser
Stelle nicht voranschreitet.
Alternativ zu wait_for_button() gibt es auch die Funktion wait_for_any_button() .
Diese pausiert das Programm auch, aber diesmal solange bis ein beliebiger
Knopf gedrückt wurde und gibt dann die Nummer des Buttons zurück. Das
ist in der Befehlsreferenz daran erkennbar, dass vor dem Funktionsnamen
ein Datentyp angegeben ist. Wenn eine Funktion einen Rückgabewert hat,
kann man ihren Aufruf wie einen Wert behandeln und so beispielsweise
in Rechenoperationen, Vergleichen oder Wertzuweisungen an Variablen
verwenden.
Dazu ein Beispiel:

```c
int pressed_button = wait_for_any_button();
```

## (Zusatz) Block II: Einstellbares Blinken
Schreibe ein Programm, bei dem man einstellen kann, wie oft der Roboter
mit den LEDs blinken soll und auf Tastendruck damit beginnt. Button A
verringert die Anzahl, Button C erhöht sie und Button B startet die Blinkse-
quenz. Gib die Anzahl der Wiederholungen auf dem Display aus.

## Block III: Abfragen der Helligkeitssensoren
Jetzt geht es darum, die Helligkeitssensoren des Roboters regelmäßig aus-
zulesen und die gemessenen Werte auf dem Display auszugeben.
Da wir nicht nur einen, sondern gleich drei Helligkeitssensoren haben, lohnt
es sich die Werte gleichzeitig auszulesen, damit wir sie danach analysie-
ren können. Die Funktion read_line_sensors() übernimmt diese Aufgabe für
uns. Damit wir auf diese Werte angemessen zugreifen können, werden
sie in einem Array gespeichert. Diese Art von Datentyp wurde bereits in
der Einführung ausführlich behandelt und kann auch dort nachgeschlagen
werden.

> **Hinweis**
>In der Befehlsreferenz gibt es zu den jeweiligen Funktionen immer gleich passende Anwendungsbeispiele, die durchaus nützlich sind.

Gehe nun wie folgt vor:
1. Lege dir ein globales Array für die drei Sensoren an. Gib diesem einen sinnvollen Namen.
2. Nun kannst du in der loop() -Funktion mit read_line_sensors() die Sensoren
auslesen. Die Funktion erhält ein Array als Parameter. Das bedeutet,
dass man das gesamte Array übergeben kann und keinen Index an-
geben muss: (Angenommen du hast das Array zum Speichern der
Sensorwerte sensors genannt)
```c
read_line_sensors(sensors);
```
3. Gib nach dem Auslesen der Sensoren den Wert des mittleren Sen-
sors auf dem Display aus. Die Indizes der Sensoren beginnen links bei 0.
4. Verschiedene Oberflächen und Lichtverhältnisse liefern unterschiedliche 
Sensorwerte. Was würdest du z.B. erwarten, wenn ihr den Sensor
auf eine weiße bzw. schwarze Oberfläche setzt oder ihn z.B. in die Luft
haltet? Wie könnte man das tatsächliche Ergebnis erklären?

## Block IV: Unterscheiden zwischen Hell und Dunkel
Schreibe ein Programm, bei dem der UNObot:
+ die gelbe LED anschaltet, wenn der mittlere Sensor eine dunkle Linie erkennt und sie ausschaltet, wenn er eine helle Fläche sieht.
+ die rote LED anschaltet, wenn der Sensor rechts außen eine dunkle
Linie erkennt und sie ausschaltet, wenn er eine helle Fläche sieht.
+ die grüne LED anschaltet, wenn der Sensor links außen eine dunkle
Linie erkennt und sie ausschaltet, wenn er eine helle Fläche sieht.
+ Zusatz: Eine Säule für jeden Helligkeitssensor auf dem Display ausgibt,
die je nach dem wie Hell der Untergrund gerade ist, verschieden hoch
ist. (Belies dich dafür in der Referenz über die Funktion bar())

**WICHTIG**
***
Um zu erkennen, ob der Sensor gerade eine Linie erkennt, benutzt
man häufig das Konzept eines Schwellwertes. Um diesen zu berechnen
benötigt man den Wert des Sensors, wenn er sich auf einer Linie
befindet (dunkel) und den Wert, wenn er sich daneben befindet (hell).
Der Schwellwert (sw) ist hierbei mathematisch der Mittelwert dieser beiden
Zahlen. Er berechnet sich wie folgt:

$` sw = \frac{dunkel + hell}{2} `$

Man geht nun davon aus, dass sobald ein gemessener Wert größer
als dieser Schwellwert ist, befindet sich der Sensor auf einer Linie,
andernfalls nicht.
***

Du hast nun alle Grundlagen, um das Verfolgen der Linie im Parcours zu
ermöglichen. Deine nächste und letzte Aufgabe ist es schließlich, den Roboter
so zu programmieren, dass er einer Linie autonom und ohne jegliches
Eingreifen von Start bis zum Stillstand im Ziel folgen kann.

Viel Erfolg :)

