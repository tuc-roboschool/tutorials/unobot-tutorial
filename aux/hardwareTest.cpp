#include <LiquidCrystal.h>
#include <UNObot.h>


#define NOTE_C5  523
#define NOTE_E5  659
#define NOTE_G5  784
#define NOTE_C6  1047
#define REST      0


void setup() {

  UNObot_init();

  print ("Testing");
  lcd_goto_xy(0, 1);
  print ("hardware.");
  delay(1000);
  clear();
  
  print ("push");
  lcd_goto_xy(0, 1);
  print ("B0");
  wait_for_button(0);
  clear();
  print ("okay");
  delay(300);
  clear();
  
  print ("push");
  lcd_goto_xy(0, 1);
  print ("B1");
  wait_for_button(1);
  clear();
  print ("okay");
  delay(300);
  clear();
  
  print ("push");
  lcd_goto_xy(0, 1);
  print ("B2");
  wait_for_button(2);
  clear();
  print ("okay");
  delay(300);
  clear();
  
  print ("checking");
  lcd_goto_xy(0,1);
  print ("LEDS");
  delay(500);
  
  for(int i=0; i<=510; i+=5)
  {
    r_led_pwm(abs(255 - i));
    y_led_pwm(abs(255 - i));
    g_led_pwm(abs(255 - i));
    delay(15);
  }
  clear();
  
  print("checking");
  lcd_goto_xy(0,1);
  print ("motors");  
  for (int i=300; i> -300; i--)
  {
    set_motors(i, -i);
    delay(5);
  }
  set_motors(0,0);
  
  clear();
  print ("checking");
  delay(500);
  clear();
  print ("u-sonic");
  lcd_goto_xy(0,1);
  print ("sensor");
  delay(1000);
  
  int dist = get_distance();
  for (int i=0; i<30; i++)
  {
    clear();
    dist = get_distance();
    print (dist);
    delay(100);
  }
  
  clear();
  print ("checking");
  delay(500);
  clear();
  print ("line");
  lcd_goto_xy(0,1);
  print ("sensors");
  delay(1000);
  
  int values[3];
  read_line_sensors(values);
  
  for (int i=0; i<30; i++)
  {
    clear();
    read_line_sensors(values);
    bar(0, 200, values[0], 0, 0);
    bar(0, 200, values[1], 3, 0);
    bar(0, 200, values[2], 6, 0);
    delay(100);
  }
  
  clear();
  print ("checking");
  lcd_goto_xy(0,1);
  print ("buzzer");
  delay(1000);

  buzzer(NOTE_C5,160);
  delay(200);
  buzzer(NOTE_E5,160);
  delay(200);
  buzzer(NOTE_G5,160);
  delay(200);
  buzzer(NOTE_C6,320);
  delay(200);
  
  
  clear();
  
  lcd_goto_xy(1,0);
  print("Done!");
  lcd_goto_xy(1,1);
  print("(^_^)");
  
}


void loop() {

}