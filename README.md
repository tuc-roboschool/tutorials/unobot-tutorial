# Willkommen bei der RoboSchool!

Wir hoffen, dass dein Roboter gut bei dir angekommen ist! Im Folgenden werden wir dich Schritt für Schritt begleiten und anleiten, um deinen UNObot selbstständig programmieren zu können.

![](./assets/UNObot.png) *Der UNObot im Detail*  

## Voraussetzungen
Das Herzstück deines Roboters ist ein sogenannter Arduino Uno. Um diesen zu programmieren, benötigst du folgendes Programm, die [Arduino IDE](https://www.arduino.cc/en/main/software). Lade dir dieses Programm herunter und installiert es auf deinem Rechner. Das ist auch schon alles, was du an Software für die Programmierung benötigst.

## Einführung
Roboter kann man relativ gut mit einem Menschen vergleichen. Hier einmal ein paar Beispiele:

|                                 | Mensch                                 | Roboter                               |
|---------------------------------|----------------------------------------|---------------------------------------|
| Input                           | Sinnesorgane (Augen, Ohren, Haut, ...) | Sensoren (Lichtsensor, Mikrofon, ...) |
| Verarbeitung                    | Gehirn                                 | Mikrocontroller (Prozessor/CPU)       |
| Weiterleitung  (Energie, Daten) | Blutbahn, Nervenbahn                   | Stromleitungen                        |
| Aktoren                         | Muskeln                                | Motoren, LED's, ...                   |
| Energie                         | Nahrung                                | Strom                                 |

### Vom Programm bis auf den Mikrocontroller
Damit du nun deinem Roboter sagen kannst was er tun soll, musst du zunächst verstehen welche Sprache der Roboter überhaupt spricht. Das Herzstück deines Roboters ist der Arduino Uno, auf welchem ein Mikroprozessor verbaut ist. Dieser arbeitet Befehle sequenziell, also nacheinander ab. Diese Befehle sind in sogenannter Maschinensprache verfasst, also Aneinanderreihungen von 0 und 1. Da dies aber für uns Menschen nur schwer verständlich ist, haben sich für uns leserliche Programmiersprachen (wie z.B. C/C++, Java, Python, ...) entwickelt. In unserem Fall benutzen wir die Sprache C. Jetzt ist nur noch die Frage, wie wir von der für uns leserlichen Programmiersprache zum Maschinencode kommen. Dafür gibt es ein hilfreiches Tool, den Compiler. Dieser ist ein Programm, welches unseren Code auf syntaktische, also grammatikalische Fehler überprüft und anschließend in maschinenlesbaren Code umwandelt. Diesen Code können wir dann auf den Arduino hochladen. Hier das zusammengefasste Schema:

![](./assets/code-flow.png)

### Arduino IDE
Alle im letzten Kapitel genannten Schritte vom Programm bis zum Mikrocontroller kannst du mithilfe der Arduino IDE ausführen. Hier eine kleine Zusammenfassung über die wichtigsten Funktionen der Arduino IDE:

**Grundlegender Aufbau:**
![](./assets/ide_overview.png)

**Grundlegende Funktionen:**

![](./assets/icon_compile.png) **Kompilieren-Schaltfläche (STRG+R):**   
Startet den Compiler, in dem grünen Kasten unten sieht man den Fortschritt und ob das Kompilieren funktioniert hat. Der Compiler macht beim Übersetzen eine Syntax-Prüfung. Das heißt, es wird geprüft ob sämtliche Befehle, Konstanten und so weiter die in deinem Code vorkommen in der Programmiersprache vorhanden sind (Achtung der Compiler prüft nicht ob das was du geschrieben hast auch Sinn ergibt, nur ob es möglich ist!). Findet er ein Fehler wird der grüne Kasten rot und nennt den ersten gefunden Fehler. Im schwarzen Kasten darunter findest du genauere Informationen zu deinem Fehler wie Zeilennummer, Position und Hinweisen, was der Compiler eventuell erwartet hätte.

![](./assets/icon_upload.png) **Hochladen-Schaltfläche (STRG+U):**  
Startet den Compiler und versucht den Maschinencode auf den Arduino zu laden. Dafür muss die Arduino IDE die Art des Arduino Boards (es handelt sich um einen Arduino UNO) und den (USB-)Port kennen an dem der Arduino mit dem Rechner verbunden ist. Grundsätzlich muss das Board mit Hilfe des Übertragungskabels an einen USB-Anschluss des Rechners angeschlossen sein. Der Boardtyp und der Port müssen unter Werkzeuge>Board (wähle „Arduino/Genuino Uno“) bzw. Werkzeuge>Port eingestellt werden. Den Port vergibt der Windows USB-Treiber automatisch, er kann sich bei jedem an und Abstecken des Verbindungskabels ändern. Unter Werkzeuge>Port siehst du eine Liste mit allen Ports deines Rechners (in der Regel nach dem Schema COM und da- hinter irgendeine Zahl), wähle den Port hinter dem in Klammern „Arduino Uno“ steht. Den gewählten Boardtyp und Port kannst du unten rechts im Fenster der IDE ablesen. Sollte der USB Treiber die Ports neu vergeben haben, wird der grüne Kasten wieder rot und weißt dich auf einen Fehler mit „COM-Port“ hin, gehe dann zu Werkezeuge>Port und stelle den Port neu ein. Ist der richtige Port eingestellt und es kam ein „COM-Port“-Fehler versuche erneut hochzuladen.

![](./assets/icon_new.png) **Neue Datei erstellen-Schaltfläche (STRG+N)**  

![](./assets/icon_open.png) **Datei öffnen-Schaltfläche (STRG+O)**  

![](./assets/icon_save.png) **Datei speichern-Schaltfläche (STRG+S)**

Unter *Datei* und *Bearbeiten* sind weitere Aktionen möglich.
Besonders nützlich noch:
- STRG+Z = Rückgängig
- STRG+Y = Wiederholen


Weiterführende Links:
- [Offizieller Guide für die Arduino IDE](https://www.arduino.cc/en/Guide/ArduinoUno) 


## Und jetzt?
Jetzt kann das Programmieren losgehen :ok_woman:  
Dazu haben wir dir schon eine Kleinigkeit vorbereitet. Damit du leichter auf die verschiedenen Funktionen des Roboters zugreifen kannst,
haben wir eine Bibiothek mit dem Namen UNObot.h geschrieben.
Um diese nutzen zu können musst du zunächst die Datei [UNObot.zip](aux/UNObot.zip) herunterladen.
Anschließend klickst du in deiner Arduino IDE auf Sketch -> Bibliothek einbinden -> .ZIP Bibliothek hinzufügen... und wählst die UNObot.zip aus. Fertig.

Schaue dir jetzt das Dokument [Einführung_in_C](./00_Einführung_in_C.md) an und arbeite dieses Schritt für Schritt inklusive der vorkommenden Aufgaben durch. Dies soll dir die wichtigsten Grundlagen für die Programmierung vermitteln.

Sobald du die Grundprinzipien von C verstanden hast, kannst du dich dann den weiterführenden Funktionen des Roboters widmen. Dafür haben wir das Dokument [Aufgaben_UNObot](./01_Aufgaben_UNObot.md) erstellt, welches du nun durcharbeiten solltest.

## Du brauchst Hilfe?
### FAQ
[Hier](03_FAQ.md) findest Du Antworten auf die häufigsten Fragen.

### Kontakt
Deine Frage taucht nicht in den FAQ auf? Du hast Probleme mit der Hardware oder der Lösung der Aufgaben?
Schreib uns einfach eine Mail an roboschool@tu-chemnitz.de und wir werden versuchen dir so schnell wie möglich zu helfen.

### Fehler gefunden?
Solltest du einen Fehler an der von uns zur Verfügung gestellten Hard- oder Software gefunden haben, kannst du uns das [hier](https://gitlab.com/tuc-roboschool/tutorials/unobot-tutorial/-/issues) mitteilen, indem du einen neuen 'Issue' anlegst (grüner Button oben rechts).
Versuche dabei den Fehler so genau wie möglich zu beschreiben. Am besten mit genauer Anleitung, damit wir ihn nachstellen, finden und beheben können.