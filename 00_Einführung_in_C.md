# Einführung in die imperative Programmierung mit der Programmiersprache C/C++ anhand des Arduino UNOs

## Inhalt
* [Grundlagen C/C++](#grundlagen-cc)
  1. [Variablen](#1-variablen)
  2. [Funktionen](#2-funktionen)
  3. [Kontrollstrukturen](#3-kontrollstrukturen)
  4. [Bedingungen und Operatoren](#4-bedingungen-und-operatoren)
  5. [Alternative](#5-alternative)
  6. [Schleifen](#6-schleifen)
  7. [Auswahl](#7-auswahl)
  8. [Arrays](#8-arrays)

## Grundlagen C/C++
In der README.md hast du erfahren, wie du die Arduino IDE verwenden kannst, kommen
wir jetzt zu einer Einführung in die Programmiersprache C/C++, auf der die
Programmierung des Mikrocontrollers namens Atmega 328P als Herzstück des Arduinos basiert.

### 1. Variablen
Die Grundlage aller Funktionalitäten der Programmiersprache C/C++ sind
Variablen. Variablen speichern Werte unter einem Namen. Um eine Variable
verwenden zu können, müssen ihr Datentyp und Name vorher bekannt
gemacht werden. Ein Datentyp gibt an, was in der der Variable gespeichert
werden kann und somit auch, wie viel Speicherplatz diese Variable benötigt.
Hier mal eine kleine Auflistung der hier relevanten Datentypen:
  + int → ganze Zahlen
  + float → Dezimalzahlen
  + char → Zeichen wie ’a’, ’b’ usw.
  + bool → hat zwei Zustände wahr oder falsch (siehe Bool’sche Werte)

```c
int times;          // <−− Deklaration
times = 5;          // <−− Zuweisung
int leds_on = 1;    // <−− Initialisierung = Deklaration & Zuweisung
```

Werte können mit = zugewiesen werden, eine Zuweisung kann gleich während 
der Deklaration erfolgen (Initialisierung). Da die Variable sonst keinen
festen Wert hat, bis die erste Zuweisung erfolgt, ist eine Initialisierung fast
immer sinnvoll. Eine Abfrage des Wertes ist ein Ausdruck und kann durch
Nennung des Namens erfolgen.

### 2. Funktionen
Funktionen können selbst definiert werden, um ein Programm zu unterteilen und die Teilprogramme mehrfach verwenden zu können, ohne Code
zu duplizieren. Was in der Funktion ausgeführt wird, ist in der Definition
beschrieben. Die Definition dient auch als Deklaration, falls die Funktion
vorher noch nicht deklariert wurde. Ein Aufruf einer Funktion erfolgt meist
mit konkreten Daten als Parameter. Es gibt auch parameterlose Funktionen.
Die Referenz (siehe [hier](Referenz.md)) gibt Aufschluss darüber, welche Parame-
ter die jeweilige Funktion benötigt.

```c
// Deklaration
int add(int a, int b);
// Definition
int add(int a, int b) {
    return a + b; // Rückgabewert
}
// Aufruf
int sum = add(2,5);
```

Die setup-Funktion definiert in unserem Programmcode den Startpunkt
der Ausführung des Programms. Der Funktionsname setup sagt es bereits.
Diese Funktion ist für alle Anweisungen gedacht, die am Anfang nur einmal
ausgeführt werden müssen (z.B. für das Initialisieren des Displays). Der 
unbestimmte Datentyp oder auch Platzhalter void besagt, dass die Funktion
keinen Rückgabewert an das aufrufende Programm zurück liefert. Was ein
Datentyp genau ist, wird im Abschnitt Variablen erläutert.

```c
void setup() {
  // ...
}
```
#### Aufgaben
Für alle folgenden Aufgaben empfehlen wir zum Testen das Display zu
benutzen. Um dieses ansteuern zu können, benötigen wir ein paar bereits
von uns vorgefertigte Funktionen, die wir in einer sog. Bibliothek gebündelt
haben. Schau dir dafür schon einmal in [Referenz.md](Referenz.md) an, wie man eine
Bibliothek einbindet bzw. [hier](Referenz.md#lcd), wie man das LCD ansteuert.

1. Erstelle eine Funktion, die zwei ganze Zahlen miteinander multipliziert
und das Ergebnis als ganze Zahl wieder zurück gibt. Rufe diese
Funktion ein Mal auf und gib das Ergebnis auf dem Display aus.
2. Erweitere nun diese Funktion, sodass sie nicht mehr als Ergebnis eine
ganze Zahl ausgibt, sondern eine Zeichenkette der folgenden Struktur
oder ähnlich: "Erg: &lt;Ergebnis der Multiplikation&gt;". 
Hinweis: mit dem "+" Operator kannst du etwas an eine Zeichenkette anhängen.

### 3. Kontrollstrukturen
Kontrollstrukturen steuern den Ablauf des Programms. Ein Teil des Codes
(Körper) wird dabei von einem Ausdruck (Bedingung) umgeben, dessen
Wert bestimmt, ob dieser Teil bzw. wie oft er ausgeführt wird. In C/C++ gibt
es zwei Arten von Kontrollstrukturen:
* Schleifen wiederholen die Ausführung ihres Körpers, solange die Bedingung ’wahr’ ist
* Alternativen entscheiden, ob ihr Körper ausgeführt wird

Boole’sche Werte sind:
* wahr (true), wenn ihr Wert ungleich 0 ist
* falsch (false), wenn ihr Wert 0 entspricht

### 4. Bedingungen und Operatoren
Um später unser Programm zu verzweigen und auf verschiedene Eingaben
oder Daten reagieren zu können, benötigen wir dafür eine gewisse Struktur:
die Bedingungen. Eine Bedingung gibt ähnlich wie eine Funktion einen Wert
zurück. Dieser beschränkt sich hierbei aber auch entweder wahr (1) oder
falsch (0). Wir können eine einfache Bedingung wie folgt bauen:
```c
x OPERATOR y
```
Für **x** und **y** können wir nun alles mögliche einsetzen wie Variablen, Zahlen,
Zeichenketten oder Rückgabewerte von Funktionen. Für **OPERATOR** gibt
es in C eine Vielzahl an Möglichkeiten wie z.B.:
```c
<, <=, ==, !=, >=, >
```

Vergleichsoperatoren liefern 1 (true), wenn der Vergleich der Werte zutrifft
(z.B. 4 < 5), ansonsten 0 (false).

**ACHTUNG, böse Falle:** ’ ==’ ist ein Vergleich, ’ =’ ist eine Zuweisung!
Wir haben nun auch die Möglichkeit mehrere Bedingungen zu einer großen
Bedingung zusammenzufassen. Dies können wir mithilfe der logischen Ope-
ratoren oder auch logische Verknüpfungen genannt. In C gibt es folgende
logische Operatoren:

```c
&& // UND
|| // ODER
!  // NICHT
```

**Beispiel:** Wir wollen überprüfen, ob die Variable ’a’ den Wert 5 hat **und** ob
die Variable ’b’ gleichzeitig größer als 3 ist.
```c
bool bedingung = (a==5) && (b>3);
```

Diese Bedingung können wir dann in der Form später, siehe nächste Kapitel,
beispielsweise in Alternativen und Schleifen benutzen.
Allgemein gilt, wenn man zwei Bedingungen a und b verknüpft:
* a && b liefert 1, wenn sowohl a als auch b ’wahr’ ergeben
* a || b liefert 1, wenn a oder b ’wahr’ ergeben (ebenfalls, wenn beide’wahr’ ergeben).

Der dritte logische Operator ist die **Negation**. Sie dient nicht direkt der
Verknüpfung von Bedingungen, sondern sie invertiert Wahrheitswerte. Ist
z.B. b ’falsch’, dann wäre !b ’wahr’ und umgekehrt.

```c
a = !b // in a wird nun das Gegenteil von b gespeichert
```

### 5. Alternative

```c
if (Bedingung) {
  // wird ausgeführt, wenn die Bedingung "wahr" ist
}
else {
  // wird ausgeführt, wenn die Bedingung "falsch" ist
  // der else−Teil muss nicht vorhanden sein
}
```

### 6. Schleifen

```c
while (Bedingung) {
  // wird ausgeführt, solange "Bedingung" zu Beginn der Schleife
  // wahr ist
}

for (Initialisierung; Bedingung; Fortsetzung) {
  // wird ausgeführt, solange "Bedingung" wahr ist
}
```

* Anweisung in Initialisierung wird vor der Schleife einmal ausgeführt
* Ausdruck in Bedingung wird überprüft, bevor der Schleifenkörper ausgeführt wird
* Anweisung in Fortsetzung wird nach jedem Durchlauf des Schleifenkörpers einmal ausgeführt

Häufig wird die for-Schleife als Zählschleife verwendet, wie in diesem Beispiel:
```c
for (int i=0; i<5; i++) {
  print(i);
}
// Ausgabe : 01234
```

#### Aufgaben
1. Erstelle eine Funktion, der du eine ein- bis zweistellige Zahl x übergibst.
Die Funktion soll nun wie ein Timer funktionieren und auf dem Display
oben links im Abstand von einer Sekunde alle Zahlen von x bis 0
ausgeben. Führe diese Funktion **einmal** aus.

2. Erstelle einen Programm, das auf dem Display die Zeit ab Start in
Sekunden angibt.

3. Erstelle eine Funktion, die als Parameter eine Zahl nimmt und die
Fakultät dieser Zahl zurückgibt.

**Hinweis:** Die Fakultät wird in der Wahrscheinlichkeitstheorie benutzt,
um Möglichkeiten von Kombinationen auszurechnen. Beispiel: Man
möchte ausrechnen, wie viele Möglichkeiten es gibt 4 verschiedene
Stifte nebeneinander anzuordnen, so bekommt man dies mithilfe der
Fakultät heraus. Man schreibt dann: 4! = 4 ∗ 3 ∗ 2 ∗ 1 = 24. Also gibt
es insgesamt 24 Möglichkeiten die 4 Stifte anzuordnen. Die Fakultät
ist also einfach eine andere Schreibweise für das Multiplizieren aller
Zahlen von der gewünschten Zahl abwärts bis 1. Aber **Achtung:** 0! = 1
und die Fakultät von negativen Zahlen ist verboten!

4. **Expertenaufgabe:** Erstelle wie in Aufgabe 3 einen Programm, das die
Zeit ab Start angibt, jedoch jetzt in Minuten, Sekunden und Millisekun-
den aufgeteilt.

**Hinweis:** Dafür sollte der Modulo-Operator (%) verwendet werden. Er
errechnet bei einer ganzzahligen Division den Rest.
Beispiel: 4 % 3 = 1

### 7. Auswahl
```c
switch (Ausdruck) {
  case Wert1:
    // wird ausgeführt, wenn Ausdruck == Wert1
    break;
  case Wert2:
  case Wert3:
    // wird ausgeführt, wenn Ausdruck dem Wert2 oder Wert3
    entspricht
    break;
  default:
    // wird ausgeführt, wenn "Ausdruck" bisher auf keinen Wert gesetzt wurde
}
```

### 8. Arrays

Arrays speichern Werte, ähnlich wie Variablen, können aber mehrere Werte
eines Typs beinhalten. Die Anzahl der Werte muss fest sein und bei der
Deklaration angegeben werden. Die einzelnen Werte können dann mit dem
Index-Operator [ ] abgerufen und zugewiesen werden. Die Indizes gehen
dabei von 0 bis Anzahl-1. Achtung: C/C++ überprüft nicht, ob die Indizes
über die Array-Grenzen hinausgehen!

```c
int values[5];              // <−− Deklaration
values[0] = 5;              // <−− Zuweisung
int middle = values[2];     // <−− Initialisierung = Dekl. & Zuweisung
int array [3] = {1, 2, 3};  // <−− Zuweisung aller Werte des Arrays
```

#### Aufgaben

1. Erstelle ein Array mit beliebig vielen Einträgen und fülle diese mit
Zahlen. Berechne nun mit einer Schleife die Summe aller Einträge und
gib diese auf dem LCD aus.

2. **Achtung, advanced:** Erstelle ein zweidimensionales Array, wobei jedes 
Feld ein Zeichen auf dem Display repräsentiert. Fülle jeden Index
der Matrix mit einem Buchstaben (Es ist hilfreich zur Wiedererkennung 
sinnvolle Wörter zu speichern). Greife auf jeden Index der Matrix
zu und gib den Buchstaben im korrespondierenden Feld auf dem LCD
aus.

### Wie geht es weiter?
Nachdem du nun einen ersten Überblick über die Programmierung mit C bekommen hast, kannst du
nun mit den [Aufgaben für den UNObot](01_Aufgaben_UNObot.md) fortfahren.
