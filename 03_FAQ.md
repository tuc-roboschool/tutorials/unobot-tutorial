# FAQ
An dieser Stelle werden wir die häufigsten Fragen sammeln und beantworten.

## Inhalt
[Ich glaube ein Teil ist kaputt. Wie kann ich das überprüfen?](#ich-glaube-ein-teil-ist-kaputt-wie-kann-ich-das-überprüfen)
[]()

### Ich glaube ein Teil ist kaputt. Wie kann ich das überprüfen?
Für diesen Fall haben wir ein Programm geschrieben.
Du findest den Code [hier](aux/hardwareTest.cpp). Das Programm testet nacheinander alle Funktionendes UNObots.. So kannst du am schnellsten herausfinden, ob etwas mit deinem Roboter nicht stimmt.
Kopiere dazu einfach den Code aus der Datei in deine Arduino IDE und lade das Ganze auf deinen UNObot.
